@extends('layouts.organizer')

@section('css')


@endsection


@section('content')
<br>

<div class="container">
    <div class="row">
        <div class="col">
            <h3 class="h-block">Nilai Peserta</h3>
            <h3 class="h-block">{{$kategori->name}}</h3>
        </div>
    </div>
</div>
<br>
<div class="container">

    <div class="card">
            <div class="card-body">
            <form class="" action="/organizer/lomba/{{$lomba->id}}/download/penilaian_simpan" method="post">
              {{csrf_field()}}
              <table class="table">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Juri 1</th>
                          <th>Juri 2</th>
                          <th>Juri 3</th>
                          <th>Rata2</th>
                          <th>Juara</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php
                      $pesertas = \App\LombakuPeserta::where('kategori_id', $kategori->id)->orderBy('ratarata', 'desc')->get();
                      $i=0;
                      $nilai_count = 0;
                  ?>
                      @foreach($pesertas as $peserta)
                      
                      <tr>
                          <th scope="row"  name="undian[]">{{$peserta->no_undian}}</th>
                          <td>
                              <input min="0" max="100" class="form-control" name="nilai1[]" type="number" value="{{$peserta->nilai1}}">
                              <input class="form-control" name="id[]" type="hidden" value="{{$peserta->id}} ">
                              <input class="form-control" name="kategori_id" type="hidden" value="{{$kategori->id}} ">

                          </td>
                          <td>
                              <input min="0" max="100" class="form-control" name="nilai2[]" type="number" value="{{$peserta->nilai2}}">
                          </td>
                          <td>
                              <input min="0" max="100" class="form-control" name="nilai3[]" type="number" value="{{$peserta->nilai3}}">
                          </td>
                          <td>
                              {{number_format($peserta->ratarata,2)}}
                          </td>
                          <td>
                              <select name="juara[]" id="juara{{$peserta->id}}" class="form-control" id="exampleFormControlSelect1">
                                  <option value="">Pilih Juara</option>
                                  <option value="1">Juara 1</option>
                                  <option value="2">Juara 2</option>
                                  <option value="3">Juara 3</option>
                                  <option value="4">Juara Harapan 1</option>
                                  <option value="5">Juara Harapan 2</option>
                                  <option value="6">Juara Harapan 3</option>
                              </select>
                          </td>

                      </tr>
                      @endforeach

                  </tbody>

              </table>
              <div align="right">
                  <a href="/organizer/lomba/{{$lomba->id}}/download/penilaian" class="btn btn-primary waves-effect waves-light">Back</a>
                  <button type="submit" class="btn btn-success waves-effect waves-light" name="button"> Simpan</button>
              </div>
              </div>
            </form>


    </div>
    <br>

</div>

<br>

@endsection


@section('js')
<script type="text/javascript">
  @foreach($pesertas as $peserta)

    $('#juara{{$peserta->id}}').val("{{$peserta->juara}}");
  @endforeach
</script>


@endsection
