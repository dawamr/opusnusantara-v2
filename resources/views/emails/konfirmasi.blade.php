<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Payment Confirmation</h2>

<div>
<h4>Please Pay the Selected Amount with 3 Unique Digit for Identification</h4>

<h3>Rp {{number_format($biaya,2)}}</h3>

<br>
<h4>CIMB Niaga Cabang Sultan Agung Semarang</h4>
<h4>No: 702509341500</h4>
<h4>Eleonora Aprilita Simanjuntak</h4>

<p>Confirmation of full payment will be sent to you within 24 hours.</p>

<a href="http://opusnusantara.com/lombaku/{{$id}}/pembayaran">Click here to confirm your Payment<a>
</div>

</body>
</html>