<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
<h1 align="center">Rekap Peserta</h1>
<h2 align="center">{{$lomba->name}}</h2>
<br />

<?php
$lombaku = DB::table('lombakus')
    ->join('users', 'users.id', '=', 'lombakus.user_id')
    ->where('lomba_id', $lomba->id)
    ->select('lombakus.*', 'users.name')
    ->orderBy('name', 'asc')
    ->get();
$i = 1;

// dd($lombaku);
?>

<table>

    <tr>
        <th >No</th>
        <th >Nama Lengkap</th>
        <th >Lahir</th>
        <th >Alamat</th>
        <th >Sekolah</th>
        <th >Kelas</th>
        <th >Guru Pembimbing</th>
        <th >No HP</th>
        <th >Daftar</th>
    </tr>
    @foreach($lombaku as $lomba)

    <?php
    $pesertas = \App\LombakuPeserta::where('lombaku_id', $lomba->id)
        ->groupBy('lomba_peserta_id')
        ->select('*', DB::raw('count(*) as total'))
        ->orderBy('nama', 'asc')
        ->get();
    // dd($pesertas);

    $i = 0;
    $total = 0;
    ?>

        @foreach($pesertas as $peserta)
            <tr>
                <td width="25px">{{$i++}}</td>
                <td width="165px">{{$peserta->nama}}</td>
                <td width="25px">{{$peserta->tanggal_lahir}}</td>
                <td width="25px">{{$peserta->alamat}}</td>
                <td width="100px">{{$peserta->sekolah_nama}}</td>
                <td width="50px">KELAS {{$peserta->sekolah_tingkat}}</td>
                <?php
// $pesertaX = DB::table('lombaku_pesertas')
//     ->join('lombakus', 'lombakus.id', '=', 'lombaku_pesertas.lombaku_id')
//     ->select('lombaku_pesertas.*', 'lombakus.user_id')
//     ->where('user_id', $x->user->id)
//     ->distinct()
//     ->get();
?>
                <td width="125px">{{$lomba->name}}</td>
                <td width="25px">{{$peserta->nohp}}</td>
                <td width="25px">{{$peserta->total}}</td>
                <?php $total = $total + $peserta->total; ?>
              
            </tr>
        @endforeach

    <tr>
       
        <td width="25px" colspan="7" align="right"><strong>TOTAL</strong></td>
        <td width="100px"><strong>{{$total}}</strong></td>
    </tr>

    @endforeach


 
</table>
<br />
